/**
 * @file game.c
 * @author Chevelu Jonathan (jonathan.chevelu@irisa.fr)
 * @brief Dedalus game controller.
 * @version 0.1
 * @date 2019-01-25
 *
 * @copyright Copyright (c) 2019
 *
 */

#include <stdio.h>   // printf
#include <string.h>  // strcpy, strlen
#include <unistd.h>  // usleep
#include <sys/types.h>
#include <sys/wait.h>
#include <stdlib.h> //exit
#include <signal.h> //kill

#include "display.h"  // outputs of the game
#include "game.h"     // public defintions
#include "gps.h"      // pos_t

/**********************************/
// Declaration of local functions.

/**
 * @brief Structure to store moves propositions.
 *
 */
 
 typedef struct {
  bool isAlive;
  compass_t move;
  bool cheated;
  pid_t characterPID;
  bool isFighting;
 }characterLink;
 
 typedef struct {
  pid_t fightPID;
  character_t* fighter1;
  character_t* fighter2;
 }fight;
 
typedef struct moves_prop {
  character_t* c;  ///< Pointer to the character concerned.
  compass_t move;  ///< The move proposition
  bool cheated;    ///< Is this a cheat move.
} moves_prop_t;

/**
 * @brief Initialize a moves propositions array.
 *
 * @param[in] pGame The game considered.
 * @param[out] pMoves Allocated array of moves propositions for each character.
 * @param[out] pNbChar Size of the array.
 * @warning The returned array must be free.
 */
void _game_init_moves_prop(const game_t* pGame,
                           moves_prop_t** pMoves,
                           size_t* pNbChar);

/**
 * @brief Get all moves propositions for each character.
 *
 * @param[in] pGame The game considered.
 * @param[in,out] pMoves An initialized vector of propositions.
 * @param[in] nbChar Number of character considered.
 * @note The array pMoves must be initialized before the call.
 */
void _game_get_moves_propositions(game_t* pGame,
                                  moves_prop_t* pMoves,
                                  size_t nbChar,
                                  characterLink** listeCharacter,
                                  fight* listeFight,
                                  size_t  nbFight);

/**
 * @brief Solve conflicts between an array of move propositions.
 *
 * @param[in] pGame The game considered.
 * @param[in,out] moves The array of moves propositions to clear.
 * @param[in] nbChar Number of character considered.
 *
 * The "moves" arrays will be cleaned to remove conflicts.
 * A conflict occurs when two characters want to go at the same position.
 * In this version only one character will move (players firsts and in order
 * of declaration after).
 */
void _game_solve_moves_conflicts(const game_t* pGame,
                                 moves_prop_t* moves,
                                 size_t nbChar);

/**
 * @brief Set a character as dead for this game.
 *
 * @param[in,out] pGame The game to change.
 * @param[in] pC The character to define has dead.
 */
void _game_death_caractere(game_t* pGame, character_t* pC);

/**
 * @brief A player exits the dedalus.
 *
 * @param[in, out] pGame The game to change.
 * @param[in, out] pC The player that exits.
 * @note The character must be a player. Otherwise it exits with a fatal error.
 */
void _game_exit_character(game_t* pGame, character_t* pC);

/**
 * @brief Says if two character should fight eachother.
 *
 * @param[in] pC1 A character.
 * @param[in] pC2 A character.
 * @return true A fight should take place.
 * @return false No fight possible.
 * @note A fight is possible if the characters are not dead, not of the same
 * type (one player vs one minotaur) and if they are at the same or an adjacent
 * position.
 */
bool _game_should_fight(const character_t* pC1, const character_t* pC2);

/**
 * @brief Run and display a fight between two characters.
 *
 * @param[in, out] pGame The game to change.
 * @param[in, out] pC1 First opponent.
 * @param[in, out] pC2 Second opponent.
 * @note This function never checks if the two characters should fight.
 */
void _game_fight(game_t* pGame, character_t* pC1, character_t* pC2, fight* listeCombat,  size_t nbFightMAX);

/**
 * @brief Check and run all fights for a given character.
 *
 * @param[in, out] pGame The game to change.
 * @param[in, out] pC The concerned character.
 */
void _game_fight_manager_char(game_t* pGame, character_t* pC, fight* listeCombat,  size_t nbFightMAX);

/**
 * @brief Check and run all fights for all characters.
 *
 * @param[in, out] pGame The game to change.
 */
void _game_fight_manager(game_t* pGame, fight* listeCombat, size_t nbFightMAX);

/**
 * @brief Define and set the target for a given character according to the board
 * configuration.
 *
 * @param[in, out] pGame The game to change.
 * @param[in] pC The charactere to find a target.
 * @return pos_t The position of the target for this character.
 * @note The target is the closest opponent if any. Else it is the closest exit
 * for a player.
 */
pos_t _game_character_target(game_t* pGame, const character_t* pC);

/**
 * @brief Play a character move
 *
 * @param[in, out] pGame The game to change.
 * @param[in,out] pC The character to play.
 * @param[in] move The move to play.
 * @param[in] cheated The AI tries to cheat when move proposition was asked.
 * @param[in] cheated Says if the AI tries to cheat.
 */
void _game_play_character(game_t* pGame,
                          character_t* pC,
                          compass_t move,
                          bool cheated);

/**
 * @brief Play all characters according to a set of moves.
 *
 * @param[in,out] pGame The game to change.
 * @param[in] moves Array of moves to play.
 * @param[in] nbChar Number of characters considered.
 */
void _game_play_characters(game_t* pGame,
                           const moves_prop_t* moves,
                           size_t nbChar);

/**
 * @brief Refresh display for everybody.
 *
 * @param[in] pGame The game to display.
 */
void _game_play_refresh_ui(const game_t* pGame);

/**
 * @brief Compute the end game status (win or loose) for the game master.
 *
 * @param[in] pGame The game considered.
 * @return ending_t An ending for the GM.
 */
ending_t _game_ending_gm(const game_t* pGame);

/**
 * @brief Compute the end game status (win or loose) for a player.
 *
 * @param[in] pGame The game considered.
 * @param[in] pC The player considered.
 * @return ending_t An ending for the player.
 *
 * Returns the global ending i.e. how successful was the player team
 * considering the player personal result.
 */
ending_t _game_ending_player(const game_t* pGame, const character_t* pC);

/*****************************/
// Functions implementation.

// Nouvelle fonction pour testé si un personage est en combat

bool _isFighting(fight* listeFight, character_t* pC, size_t nbFight){
	bool isFighting = false;
  	size_t index = 0;
  	while(index < nbFight && (listeFight[index].fighter1) != NULL && (listeFight[index].fighter2) != NULL && !isFighting ){
  		isFighting = ((pC == (listeFight[index].fighter1)) || (pC == (listeFight[index].fighter2)));
  		index ++;
  	}
  	return isFighting;
}

//

void _game_init_moves_prop(const game_t* pGame,
                           moves_prop_t** pMoves,
                           size_t* pNbChar) {
  *pNbChar = pGame->nbPlayer + pGame->nbMinotaur;
  *pMoves = (moves_prop_t*)malloc((*pNbChar) * sizeof(moves_prop_t));
  if (*pMoves == NULL) {
    display_fatal_error(stderr, "Error: malloc failed!");
    exit(EXIT_FAILURE);
  }

  // Init

  for (size_t i = 0; i < pGame->nbPlayer; ++i) {
    (*pMoves)[i].c = &(pGame->playerA[i]);
    (*pMoves)[i].cheated = false;
    (*pMoves)[i].move = Stay;
  }
  size_t offset = pGame->nbPlayer;
  for (size_t i = 0; i < pGame->nbMinotaur; ++i) {
    (*pMoves)[i + offset].c = &(pGame->minotaurA[i]);
    (*pMoves)[i + offset].cheated = false;
    (*pMoves)[i + offset].move = Stay;
  }
}

void _game_death_caractere(game_t* pGame, character_t* pC) {
  switch (pC->type) {
    case PLAYER:
      pGame->nbPlayerAlive--;
      pGame->nbPlayerOnBoard--;
      break;
    case MINOTAUR:
      pGame->nbMinotaurAlive--;
      break;
    default:
      display_fatal_error(DISPLAY, "Try to kill something strange\n");
      exit(EXIT_FAILURE);
  }
  character_is_dead(pGame->pMap, pC);
}

void _game_exit_character(game_t* pGame, character_t* pC) {
  switch (pC->type) {
    case PLAYER:
      pGame->nbPlayerOnBoard--;
      break;
    default:
      display_fatal_error(DISPLAY, "Try to exit something strange\n");
      exit(EXIT_FAILURE);
  }
  character_is_out(pGame->pMap, pC);
}

bool _game_should_fight(const character_t* pC1, const character_t* pC2) {
  // not dead and same position or next to eachother
  return ((pC1->type != DEAD) && (pC2->type != DEAD) &&
          (((pC1->pos.x + 1 == pC2->pos.x) && (pC1->pos.y == pC2->pos.y)) ||
           ((pC1->pos.x - 1 == pC2->pos.x) && (pC1->pos.y == pC2->pos.y)) ||
           ((pC1->pos.x == pC2->pos.x) && (pC1->pos.y + 1 == pC2->pos.y)) ||
           ((pC1->pos.x == pC2->pos.x) && (pC1->pos.y - 1 == pC2->pos.y)) ||
           ((pC1->pos.x == pC2->pos.x) && (pC1->pos.y == pC2->pos.y))));
}

void _game_fight(game_t* pGame, character_t* pC1, character_t* pC2, fight* listeCombat,  size_t nbFightMAX) {
	size_t currsor = 0;
    	while(listeCombat[currsor].fightPID != 0 && currsor<nbFightMAX){
    		currsor++;
    	}
    	(listeCombat[currsor]).fighter1 = pC1;
  	(listeCombat[currsor]).fighter2 = pC2;
    	pid_t filsFigthPID = -1;
    	while( filsFigthPID == -1 ){
    		filsFigthPID = fork();
    	}

  	if(filsFigthPID == 0){
  		//sleep(100);
  		size_t i = 0;
  		while ((pC1->health > 0) && (pC2->health > 0)) {
  		  display_fight_iteration(pGame->pMap, pC1, pC2, (size_t)pGame->delay, i);
  		  usleep((unsigned int)pGame->delay);
  		  ++i;
  		  --(pC1->health);
  		  --(pC2->health);
  		}
  		display_fight_iteration(pGame->pMap, pC1, pC2, (size_t)pGame->delay, i);
		
/*		  if (pC1->health <= 0) {
		    _game_death_caractere(pGame, pC1);
		  }
		
		  if (pC2->health <= 0) {
		    _game_death_caractere(pGame, pC2);
		  }*/
		                    
		  
		  
		  int status;
		  
		  if(pC1->health == 0){
		  	
		  	_game_death_caractere(pGame, pC1);
		  	
		  	//_game_play_refresh_ui(pGame);
		  	
		  	display_wait_user(DISPLAY, "Press any key to continue...",
		                    pGame->interactive);
		
			// refresh display
			display_fight_clear(pC1->stream);
			display_ui_player(pGame->gameName, pC1->ai.name, pGame->pMap, pC1,
			                    pGame->delay, pGame->maxMoves, pGame->gameInfo, false,
		                    false);
			display_fight_clear(pC2->stream);
			display_ui_player(pGame->gameName, pC2->ai.name, pGame->pMap, pC2,
		                    pGame->delay, pGame->maxMoves, pGame->gameInfo, false,
		                    false);
		  	
		  	status =(int)(pC2->health);
		  	
		  	exit(status);
		  }
		  else if(pC2->health == 0){
		  
		  	_game_death_caractere(pGame, pC2);
		  	
		  	//_game_play_refresh_ui(pGame);
		  	
		  	display_wait_user(DISPLAY, "Press any key to continue...",
		                    pGame->interactive);
		
			// refresh display
			display_fight_clear(pC1->stream);
			display_ui_player(pGame->gameName, pC1->ai.name, pGame->pMap, pC1,
			                    pGame->delay, pGame->maxMoves, pGame->gameInfo, false,
		                    false);
			display_fight_clear(pC2->stream);
			display_ui_player(pGame->gameName, pC2->ai.name, pGame->pMap, pC2,
		                    pGame->delay, pGame->maxMoves, pGame->gameInfo, false,
		                    false);
		  	
		  	status =(int)(150 + pC1->health);
		  
		    	exit(status);
		  }
		  else{
		  	exit (0);
		  }
  	}	
  	else{// code du père
  		(listeCombat[currsor]).fightPID = filsFigthPID;
  	}
}

void _game_fight_manager_char(game_t* pGame, character_t* pC, fight* listeCombat,  size_t nbFightMAX) {  // TODO V4 rendre les combat non bloquant
  character_t* pOpponents = NULL;
  size_t nbOpponents = 0;
  switch (pC->type) {
    case PLAYER:
      pOpponents = pGame->minotaurA;
      nbOpponents = pGame->nbMinotaur;
      break;
    case MINOTAUR:
      pOpponents = pGame->playerA;
      nbOpponents = pGame->nbPlayer;
      break;
    default:
      break;
      // No opponent
  }
  for (size_t i = 0; i < nbOpponents; ++i) {
    if (_game_should_fight(pC, &(pOpponents[i])) &&
     !_isFighting(listeCombat, pC, nbFightMAX) &&
     !_isFighting(listeCombat, &(pOpponents[i]), nbFightMAX)) {
    	_game_fight(pGame, pC, &(pOpponents[i]), listeCombat, nbFightMAX);
    }
  }
}

void _game_fight_manager(game_t* pGame, fight* listeCombat, size_t nbFightMAX) {
  // Players fight
  for (size_t i = 0; i < pGame->nbPlayer; ++i) {
    _game_fight_manager_char(pGame, &(pGame->playerA[i]), listeCombat, nbFightMAX);
  }

  // Minotaur fight (notice : should be useless)
/*  for (size_t i = 0; i < pGame->nbMinotaur; ++i) {
    _game_fight_manager_char(pGame, &(pGame->minotaurA[i]), listeCombat, nbFightMAX);
  }*/
}

pos_t _game_character_target(game_t* pGame, const character_t* pC) {
  pos_t target;

  switch (pC->type) {
    case PLAYER:
      if ((pGame->finalLevel) && (pGame->nbMinotaurAlive > 0)) {
        target =
            gps_closest(pC->pos, pGame->minotaurA, pGame->nbMinotaur, MINOTAUR);
      } else {
        target = gps_closest(pC->pos, pGame->exitA, pGame->nbExit, EXIT);
      }
      break;
    case MINOTAUR:
      target = gps_closest(pC->pos, pGame->playerA, pGame->nbPlayer, PLAYER);
      break;
    default:
      // No target
      target = pC->pos;
  }
  return target;
}

void _game_get_moves_propositions(game_t* pGame,
                                  moves_prop_t* pMoves,
                                  size_t nbChar,
                                  characterLink** listeCharacter,
                                  fight* listeFight,
                                  size_t  nbFight) {
  characterLink* listeFils = *listeCharacter;
  int inputStatus;
  int outputStatus;
  for (size_t i = 0; i < nbChar; ++i) {
    if( (listeFils[i]).isAlive && (listeFils[i]).characterPID == -1 && !_isFighting(listeFight, pMoves[i].c, nbFight) ){
    		pid_t PID_fils = -1;
    		while (PID_fils == -1){ // Si il y a le fork renvoi une erreur on recomence
    			PID_fils = fork();
    			if(PID_fils == 0){ // processus fils
    				bool cheat;
    				character_t* pC = pMoves[i].c;
    				inputStatus=character_propose_move(pC, pGame->pMap, &cheat);
    				if(cheat){
    					inputStatus=10+inputStatus;
    				}
    				exit(inputStatus);
    			}
    			else{ // processus père
    				(listeFils[i]).characterPID=PID_fils;
    			}
    		}
    }
  }
  pid_t fils = -1;
  size_t cursor = 0;
  fils = wait(&outputStatus);
  while( cursor<nbChar && (listeFils[cursor]).characterPID != fils){
  	(pMoves[cursor].cheated) = false;
 	pMoves[cursor].move = Stay;
  	cursor ++;
  }
  bool filsIsFighting = _isFighting(listeFight, pMoves[cursor].c, nbFight);
  
  if ( !(filsIsFighting) && cursor<nbChar && (pMoves[cursor].c)->type != DEAD ){ // Si le personage n'est pas en combat il peu bougé
  	(pMoves[cursor].cheated) = (outputStatus/10 == 1);
 	pMoves[cursor].move = (outputStatus %10);
  	(listeFils[cursor]).cheated = (outputStatus/10 == 1);
  	(listeFils[cursor]).move = (outputStatus %10);
  	(listeFils[cursor]).characterPID = -1;
  }
  else if(filsIsFighting && cursor<nbChar && (pMoves[cursor].c)->type != DEAD ){ // Si le personage est en combat il ne peu pas bougé
  	(pMoves[cursor].cheated) = false;
 	pMoves[cursor].move = Stay;
  }
  else if( cursor == nbChar && fils > 0){ // On a surement les résultat d'un combat
  	size_t fightCursor =0 ;
  	while((listeFight[fightCursor].fightPID != 0) && ( fils != (listeFight[fightCursor]).fightPID && fightCursor < nbFight)){
  		fightCursor++;
  	}
  	if(listeFight[fightCursor].fightPID != 0 && fightCursor < nbFight){ // On a le résultat d'un combat
  		if(outputStatus / 150 == 1){
  			((listeFight[fightCursor]).fighter1)->health = 0;
  			_game_death_caractere(pGame, (listeFight[fightCursor]).fighter1);
  			size_t charCursor = 0;
  			while (pMoves[charCursor].c != (listeFight[fightCursor]).fighter1){
  				++ charCursor;
  			}
  			(listeFils[charCursor]).isAlive = false;
  			((listeFight[fightCursor]).fighter2)->health = outputStatus % 150;
  			listeFight[fightCursor].fightPID = 0;
  			listeFight[fightCursor].fighter1 = NULL;
  			listeFight[fightCursor].fighter2 = NULL;
  			
  		}
  		else{
  			((listeFight[fightCursor]).fighter1)->health = outputStatus;
  			((listeFight[fightCursor]).fighter2)->health = 0;
  			_game_death_caractere(pGame, (listeFight[fightCursor]).fighter2);
  			size_t charCursor = 0;
  			while (pMoves[charCursor].c != (listeFight[fightCursor]).fighter2){
  				++ charCursor;
  			}
  			(listeFils[charCursor]).isAlive = false;
  			listeFight[fightCursor].fightPID = 0;
  			listeFight[fightCursor].fighter1 = NULL;
  			listeFight[fightCursor].fighter2 = NULL;
  		}
  		fightCursor++;
  		while(listeFight[fightCursor].fightPID != 0){
  			fight fight_tmp = listeFight[fightCursor - 1];
  			listeFight[fightCursor - 1] = listeFight[fightCursor];
  			listeFight[fightCursor] = fight_tmp;
  			fightCursor++;
  		}
  	}
  	else{ // On a un processus étrange
  		kill(fils,9);
  	}
  }
  else{
  	(listeFils[cursor]).isAlive = false;
  	(listeFils[cursor]).cheated = true;
  	(listeFils[cursor]).move = Stay;
  	(listeFils[cursor]).characterPID = 0;
  	(pMoves[cursor].cheated) = true;
 	pMoves[cursor].move = Stay;
  }
  cursor ++;
  while(cursor < nbChar){
  	(pMoves[cursor].cheated) = false;
 	pMoves[cursor].move = Stay;
  	cursor ++;
  }
}

void _game_solve_moves_conflicts(const game_t* pGame,
                                 moves_prop_t* moves,
                                 size_t nbChar) {
  (void)pGame;  // unused

  // Solve all conflicts
  bool conflictFound = false;
  do {
    pos_t* usedPositions = NULL;
    size_t nbPosUsed = 0;
    // Fix position of staying char
    for (size_t i = 0; i < nbChar; ++i) {
      character_t* pC = moves[i].c;
      // Only alive characters are interested
      if ((moves[i].move == Stay) &&
          ((pC->type == PLAYER) || (pC->type == MINOTAUR))) {
        // reserve position. We are sure there is no conflict.
        ++nbPosUsed;
        usedPositions =
            (pos_t*)realloc(usedPositions, nbPosUsed * sizeof(pos_t));
        if (usedPositions == NULL) {
          display_fatal_error(stderr, "Error: can not realloc usedPosition!");
          exit(EXIT_FAILURE);
        }
        usedPositions[nbPosUsed - 1] = pC->pos;
      }
    }

    // Search conflict for others but stop at first conflict
    // Just one conflict at a time in order to be sure that refusing a move for
    // a character will not create another conflict.
    conflictFound = false;
    size_t i = 0;
    while ((!conflictFound) && (i < nbChar)) {
      character_t* pC = moves[i].c;
      // Only alive characters are interested
      // We only consider moving characters since those staying are already
      // managed.
      if ((moves[i].move != Stay) &&
          ((pC->type == PLAYER) || (pC->type == MINOTAUR))) {
        // Compute target position
        pos_t nextPos = gps_compute_move(pC->pos, moves[i].move);

        // Check if we have a conflict
        size_t j = 0;
        while ((!conflictFound) && (j < nbPosUsed)) {
          conflictFound = ((usedPositions[j].x == nextPos.x) &&
                           (usedPositions[j].y == nextPos.y));
          ++j;
        }
        ++nbPosUsed;
        usedPositions =
            (pos_t*)realloc(usedPositions, nbPosUsed * sizeof(pos_t));
        if (usedPositions == NULL) {
          display_fatal_error(stderr, "Error: can not realloc usedPosition!");
          exit(EXIT_FAILURE);
        }
        if (conflictFound) {
          moves[i].move = Stay;
          usedPositions[nbPosUsed - 1] = pC->pos;
        } else {
          // Reserve the position
          usedPositions[nbPosUsed - 1] = nextPos;
        }
      }
      ++i;
    }

    free(usedPositions);
  } while (conflictFound);
}

void _game_play_character(game_t* pGame,
                          character_t* pC,
                          compass_t move,
                          bool cheated) {
  bool exited = false;

  if (cheated) {
    // Deal with cheater
    _game_death_caractere(pGame, pC);
    if (pGame->nbMinotaurAlive > 0) {
      pC->ending = EC_CHEAT_MINOTAUR;
    } else {
      pC->ending = EC_CHEAT_NO_MINOTAUR;
    }
  } else {
    // Move character
    pos_t target = _game_character_target(pGame, pC);
    character_play(pC, move, target, pGame->pMap, pGame->steps, pGame->maxMoves,
                   &exited);

    if ((pC->type != DEAD) && (pC->health <= 0)) {
      // Deal with exhaustion
      _game_death_caractere(pGame, pC);
      if (pGame->nbMinotaurAlive > 0) {
        pC->ending = EC_STARVE_MINOTAUR;
      } else {
        pC->ending = EC_STARVE_NO_MINOTAUR;
      }
    }
  }

  // Deal with exit
  if (exited && (pC->type == PLAYER) && (pGame->nbMinotaurAlive <= 0)) {
    // NOTE: if you kill the last Minotaur on an exit tile, you have to move
    // twice to exit.

    // NOTE: you can't exit if you are dead.
    _game_exit_character(pGame, pC);
    if (pGame->finalLevel) {
      pC->ending = EC_WIN;
    } else {
      pC->ending = EC_ESCAPE;
    }
  }
}

void _game_play_characters(game_t* pGame,
                           const moves_prop_t* moves,
                           size_t nbChar) {
  // Play all characters
  for (size_t i = 0; i < nbChar; ++i) {
  	_game_play_character(pGame, moves[i].c, moves[i].move, moves[i].cheated);
  }
}

void _game_play_refresh_ui(const game_t* pGame) {
  // Play all characters

  // Display current state for game master
  display_ui_gm(DISPLAY, pGame->gameName, pGame->pMap, pGame->nbPlayerAlive,
                pGame->nbPlayerOnBoard, pGame->nbMinotaurAlive, pGame->delay,
                pGame->gameInfo, true);
  for (size_t i = 0; i < pGame->nbPlayer; ++i) {
    const character_t* pC = &(pGame->playerA[i]);
    // Display for current player
    display_ui_player(pGame->gameName, pC->ai.name, pGame->pMap, pC,
                      pGame->delay, pGame->maxMoves, pGame->gameInfo, false,
                      true);
  }
}

ending_t _game_ending_gm(const game_t* pGame) {
  if (pGame->nbPlayerAlive > 0) {
    return EG_GM_LOOSE;
  }
  return EG_GM_WIN;
}

ending_t _game_ending_player(const game_t* pGame, const character_t* pC) {
  ending_t ending = EG_LOOSE;
  if (pC->type == EXIT) {
    if (pGame->finalLevel) {
      ending = EG_WIN_AND_ALIVE;
    } else {
      ending = EG_NEXT_LEVEL_AND_ALIVE;
    }
  } else if (pGame->nbPlayerAlive > 0) {
    if (!pGame->finalLevel) {
      ending = EG_NEXT_LEVEL_BUT_DEAD;
    } else if (pGame->nbMinotaurAlive <= 0) {
      ending = EG_WIN_BUT_DEAD;
    }  // else {} //  LOOSE
  } else if ((pGame->nbMinotaurAlive <= 0) && (pGame->finalLevel)) {
    ending = EG_KILL_MINOTAUR_BUT_DEAD;
  }
  return ending;
}

/**********************************/
// Public functions implementations.

//TODO il faut lancer les affichage des joueur

bool game_init(game_t* pGame,
               config_t* pConf,
               const char* gameName,
               map_t* pMap,
               int pDefHealth,
               ai_t pAi,
               int mDefHealth,
               ai_t mAi) {
  pGame->gameName = gameName;
  pGame->pMap = pMap;
  pGame->maxMoves = pConf->maxMoves;
  pGame->gameInfo = pConf->gameInfo;
  pGame->steps = 0;
  pGame->delay = pConf->delay;
  pGame->interactive = pConf->interactive;

  bool ok = true;

  // Load exit(s)
  pGame->nbExit = gps_locator(pMap, EXIT, &(pGame->exitA));
  bool noExit = (pGame->nbExit == 0);

  // Load a Minotaur(s)
  pos_t* mAPos = NULL;
  pGame->nbMinotaur = gps_locator(pMap, MINOTAUR, &mAPos);
  pGame->nbMinotaurAlive = pGame->nbMinotaur;
  pGame->finalLevel = (pGame->nbMinotaur > 0);
  if ((pGame->nbExit == 0) && (!pGame->finalLevel)) {
    // if no exit, then it must be a final level
    ok = false;
  }
  // NOTE: malloc(0) return NULL so it is ok
  pGame->minotaurA =
      (character_t*)malloc(pGame->nbMinotaur * sizeof(character_t));
  for (size_t i = 0; i < pGame->nbMinotaur; ++i) {
    pGame->minotaurA[i] = character_init(MINOTAUR, (pid_t)(-i - 1), false,
                                         "Minotaur", mDefHealth, mAi);
    pGame->minotaurA[i].pos = mAPos[i];
    pGame->minotaurA[i].pMask = map_mask_init(pMap);
    map_mask_add(pGame->minotaurA[i].pMask, mAPos[i]);

    // NOTE: Targets are not set yet
  }
  free(mAPos);
  mAPos = NULL;

  // Load a player(s)
  pos_t* pAPos = NULL;
  pGame->nbPlayer = gps_locator(pMap, PLAYER, &pAPos);
  pGame->nbPlayerAlive = pGame->nbPlayer;
  pGame->nbPlayerOnBoard = pGame->nbPlayer;
  if (pGame->nbPlayer == 0) {
    // Need at least one player
    ok = false;
  }
  pGame->playerA = (character_t*)malloc(pGame->nbPlayer * sizeof(character_t));
  for (size_t i = 0; i < pGame->nbPlayer; ++i) {
    // Get the display to use with this player.  // TODO moar comment +correction
    pid_t PID_player = -1;
    PID_player =fork();
    if (PID_player==0) {//code du fils
    //recouvrement du fils par un terminal xterm
      execlp("xterm","xterm",NULL);//on ouvre des terminaux xterm pour obtenir un nouvel affichage
    }
    else{//code du père
    	config_add_display(pConf,PID_player); // on ajoute un nouvel affichage pour pouvoir le supprimé par la suite
    }
	
    // Init player // TODO moar coment +correction
    sleep(1); // permet au affichage d'avoir le temps de s'ouvrir
    pGame->playerA[i] =
        character_init(PLAYER, PID_player, true, "Theseus", pDefHealth, pAi);//permet de faire l'affichage dans le fils des xterm (donc le bash)
    pGame->playerA[i].pos = pAPos[i];
    pGame->playerA[i].pMask = map_mask_init(pMap);
    map_mask_add(pGame->playerA[i].pMask, pAPos[i]);
    // NOTE: Targets are not set yet
  }
  free(pAPos);
  pAPos = NULL;

  // Add missing exit (if final)
  if (ok && noExit && pGame->finalLevel) {
    for (size_t i = 0; i < pGame->nbPlayer; ++i) {
      if ((pGame->playerA[i].pos.x == 0) || (pGame->playerA[i].pos.y == 0)) {
        // add an exit
        if (pGame->nbExit == 0) {
          pGame->exitA = (pos_t*)malloc(1 * sizeof(pos_t));
        } else {
          pGame->exitA = (pos_t*)realloc(pGame->exitA,
                                         (pGame->nbExit + 1) * sizeof(pos_t));
        }
        pGame->exitA[pGame->nbExit].x = pGame->playerA[i].pos.x;
        pGame->exitA[pGame->nbExit].y = pGame->playerA[i].pos.y;
        pGame->playerA[i].walkOn = EXIT;
        ++(pGame->nbExit);
      }
    }
  }
  if (pGame->nbExit == 0) {
    ok = false;
  }

  // Init targets
  for (size_t i = 0; i < pGame->nbPlayer; ++i) {
    character_t* pC = &(pGame->playerA[i]);
    gps_direction(pC->pos, _game_character_target(pGame, pC),
                  &(pC->targetCompass), &(pC->targetDistance));
  }
  for (size_t i = 0; i < pGame->nbMinotaur; ++i) {
    character_t* pC = &(pGame->minotaurA[i]);
    gps_direction(pC->pos, _game_character_target(pGame, pC),
                  &(pC->targetCompass), &(pC->targetDistance));
  }

  return ok;
}

void game_start(game_t* pGame) {
  // Print UI

  // Initial display for game master
  display_ui_gm(DISPLAY, pGame->gameName, pGame->pMap, pGame->nbPlayerAlive,
                pGame->nbPlayerOnBoard, pGame->nbMinotaurAlive, pGame->delay,
                pGame->gameInfo, false);

  // Initial display for each player
  for (size_t i = 0; i < pGame->nbPlayer; ++i) {
    const character_t* pC = &(pGame->playerA[i]);
    display_ui_player(pGame->gameName, pC->ai.name, pGame->pMap, pC,
                      pGame->delay, pGame->maxMoves, pGame->gameInfo, true,
                      false);
  }

  size_t nbChar = 0;
  characterLink* listeCharacter = NULL;
  moves_prop_t* moves = NULL;
  _game_init_moves_prop(pGame, &moves, &nbChar);
  listeCharacter = malloc(nbChar * sizeof (characterLink) ); // On créé une liste des personnage
  
  for(size_t i=0; i<nbChar; i++){
  	(listeCharacter[i]).isAlive = true;
  	(listeCharacter[i]).characterPID = -1;
  }
  
  //initialisation des combats
  fight* listeCombat = malloc((pGame->nbMinotaurAlive)*sizeof(fight));
  for(size_t i ; i<(pGame->nbMinotaurAlive); i++){
  	(listeCombat[i]).fightPID = 0;
  	//(listeCombat[i]).fighter1 = malloc(sizeof(character_t);
  	(listeCombat[i]).fighter1 = NULL;
  	(listeCombat[i]).fighter1 = NULL;
  }
  size_t nbFight = (pGame->nbMinotaurAlive);

  do {//TODO V4
    // Play characters
    pGame->steps += 1;
    usleep((unsigned int)pGame->delay);

    // Fights
    _game_fight_manager(pGame, listeCombat, nbFight);

    // Get characters move propositions
    
    
    //_game_get_moves_propositions(pGame, moves, nbChar, &(listeCharacter));
    _game_get_moves_propositions(pGame, moves, nbChar, &(listeCharacter), listeCombat, nbFight);

    // Solve confilcts
    _game_solve_moves_conflicts(pGame, moves, nbChar);
  	
    // Play characters
    /*size_t numCharac =0;
    
    while(numCharac<nbChar && listeCharacter->characterPID != -1){
    	numCharac++;
    }
    
    if(numCharac<nbChar){
    	_game_play_character(pGame, (moves[numCharac].c), (moves[numCharac].move), (moves[numCharac].cheated));
    }*/
    
    _game_play_characters(pGame, moves, nbChar);
    

    // refresh displays
    _game_play_refresh_ui(pGame);

    // Fights
    
    _game_fight_manager(pGame, listeCombat, nbFight);
    
    for(size_t i = 0; i < nbChar; i++){
    	if(listeCharacter[i].isAlive){
    		if((moves[i].c)->type == DEAD){
    			listeCharacter[i].isAlive = false;
    			if(listeCharacter[i].characterPID > 0 && listeCharacter[i].characterPID !=0){
    				kill(listeCharacter[i].characterPID, 9);
    			}
    			// Si un personnage est mort et que un processus sensé renvoyé un mouvement est toujour un zombie on le tue 
    			listeCharacter[i].characterPID = 0;
    		}
    		else{
    			listeCharacter[i].isAlive = true;
    		}
    	}
    }
    
    nbFight = (pGame->nbMinotaurAlive);
    
    //listeCombat = realloc((nbFight)*sizeof(fight));
    
    

  } while (pGame->nbPlayerOnBoard > 0);
  
  free(moves);
  
  nbChar = 0;
  
  free (listeCombat);
  
  free (listeCharacter);

  // The end
  for (size_t i = 0; i < pGame->nbPlayer; ++i) {
    const character_t* pC = &(pGame->playerA[i]);
    // Last display for each player
    display_ending(pGame->playerA[i].stream, _game_ending_player(pGame, pC));
  }
  display_ending(DISPLAY, _game_ending_gm(pGame));
}

void game_delete(game_t* pGame) {
  pGame->gameName = NULL;

  free(pGame->exitA);
  pGame->exitA = NULL;
  pGame->nbExit = 0;

  for (size_t i = 0; i < pGame->nbPlayer; ++i) {
    character_delete(pGame->pMap, &(pGame->playerA[i]));
  }
  free(pGame->playerA);
  pGame->playerA = NULL;
  pGame->nbPlayer = 0;
  pGame->nbPlayerAlive = 0;
  pGame->nbPlayerOnBoard = 0;

  for (size_t i = 0; i < pGame->nbMinotaur; ++i) {
    character_delete(pGame->pMap, &(pGame->minotaurA[i]));
  }
  free(pGame->minotaurA);
  pGame->minotaurA = NULL;
  pGame->nbMinotaur = 0;
  pGame->nbMinotaurAlive = 0;

  pGame->finalLevel = false;
  pGame->maxMoves = 0;
  pGame->steps = 0;
  pGame->delay = 0;

  map_delete(pGame->pMap);
  pGame->pMap = NULL;
}
